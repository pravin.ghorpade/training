﻿CREATE TABLE [dbo].[Cart]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Category] NVARCHAR(50) NOT NULL, 
    [Price] NUMERIC(16, 2) NOT NULL, 
    [Quantity] INT NOT NULL, 
    CONSTRAINT [Id] FOREIGN KEY ([Column]) REFERENCES [ToTable]([ToTableColumn])
)
