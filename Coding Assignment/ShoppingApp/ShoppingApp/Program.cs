﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp
{
    class Program
    {

        static Product1.Product1DataTable CreateProducts()
        {
            SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Product1;Integrated Security=True;Pooling=False");
            SqlDataAdapter adapter = new SqlDataAdapter("select * from Products1", conn);

            Product1.Product1DataTable table = new Product1.Product1DataTable();

            adapter.Fill(table);
            return table;
        }

        static Product1.CartDataTable CreateCart()
        {
            SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Product1;Integrated Security=True;Pooling=False");
            SqlDataAdapter adapter1 = new SqlDataAdapter("select * from Cart", conn);

            Product1.CartDataTable cartTable = new Product1.CartDataTable();
            adapter1.Fill(cartTable);
            return cartTable;
        }

        static void Main(string[] args)
        {
            int flag1 = 0;

            while (flag1!=1)
            {
                Console.WriteLine("\n**Console Base Shopping Application**\n");
                Console.WriteLine("1.Show All Products.");
                Console.WriteLine("2.Show products by Category.");
                Console.WriteLine("3.Add Product to cart");
                Console.WriteLine("4.Remove product from Cart");
                Console.WriteLine("5.Show My Cart");
                Console.WriteLine("Enter your choice.");
                
                int choice = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("");
                switch (choice)
                {
                    case 1:
                        ShowAllProducts();
                        break;
                    case 2:
                        Console.WriteLine("Categories:\n1.General Sports\n2.Cricket\n3.Football\n4.Badminton");
                        ShowProductByCategory();
                        break;
                    case 3:
                        AddToCart();
                        break;
                    case 4:
                        
                        break;
                    case 5:
                        ShowCart();
                        break;
                    default:
                        break;
                } 
            }
            flag1 = 1;
        
            Console.ReadLine();
        }

        
        


        static void ShowAllProducts()
        {
                var products = from p in CreateProducts()

                               select p;
                foreach (var item in products)
                {
                    Console.WriteLine("{0}\t{1}\t\t{2}\t\t\t\t{3}\t\t{4}",item.Id,item.Name, item.Category, item.Price,item.Quantity);
                }

        }

        static void ShowProductByCategory()
        {

            int choice1 = Convert.ToInt32(Console.ReadLine());
            switch (choice1)
            {
                case 1:
                    var products1 = from a in CreateProducts()
                                    where a.Category == "Sport"
                                    select a;
                    foreach (var item in products1)
                    {
                        Console.WriteLine("{0}\t{1}\t{2}\t{3}",item.Id,item.Name, item.Price, item.Quantity);
                    }

                    break;
                case 2:
                    var products2 = from b in CreateProducts()
                                    where b.Category == "Cricket"
                                    select b;
                    foreach (var item in products2)
                    {
                        Console.WriteLine("{0}\t{1}\t{2}\t{3}",item.Id,item.Name, item.Price, item.Quantity);
                    }
                    break;

                case 3:
                    var products3 = from c in CreateProducts()
                                    where c.Category == "Football"
                                    select c;
                    foreach (var item in products3)
                    {
                        Console.WriteLine("{0}\t{1}\t{2}\t{3}",item.Id,item.Name, item.Price, item.Quantity);
                    }
                    break;

                case 4:
                
                    var products4 = from d in CreateProducts()
                                    where d.Category == "Badminton"
                                    select d;
                    foreach (var item in products4)
                    {
                        Console.WriteLine("{0}\t{1}\t{2}\t{3}",item.Id,item.Name, item.Price, item.Quantity);
                    }
                    break;

                default:
                    break;
            }
        }

        static void AddToCart()
        {
            Console.WriteLine("Enter the ID of the product:");
            int id = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("How many quantity do you want?");
            int quantity = Convert.ToInt32(Console.ReadLine());

            var products5 = from e in CreateProducts()
                            where e.Id == id
                            select e;
            foreach (var item in products5)
            {
                string insertCommand = string.Format("insert into Cart" + "(Id,Name,Price,Quantity,Total)" +
                  "values('{0}','{1}','{2}','{3}','{4}')", item.Id, item.Name, item.Price, quantity, item.Price*quantity);

                using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Product1;Integrated Security=True;Pooling=False"))
                {
                    conn.Open();

                    SqlCommand command = new SqlCommand();
                    command.CommandText = insertCommand;
                    command.CommandType = System.Data.CommandType.Text;
                    command.Connection = conn;

                    var result = command.ExecuteNonQuery();

                    if (result > 0)
                    {
                        Console.WriteLine("Product Added to Cart Successfully..!!");
                    }
                    else
                    {
                        Console.WriteLine("Some Problem with Adding Product..!!");
                    }

                    conn.Close();
                }


            }

        }

        static void ShowCart()
        {
            var cart = from c in CreateCart()

                           select c;

            decimal subtotal=0;
            foreach (var item in cart)
            {

                subtotal += item.Total;
                Console.WriteLine("{0}\t{1}\t\t{2}\t{3}\t{4}",item.Id,item.Name,item.Price,item.Quantity,item.Total);

                
            }
            Console.WriteLine("Total amount of the Cart is:{0}", +subtotal);
        }

        static void RemoveCart()
        {
            var cart1 = from f in CreateCart()
                        select f;
            foreach (var item in cart1)
            {
                
            }
        }
    }


}
