﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class LoginEventArgs
    {
        public string Uname { get; set; }
        public string Pass { get; set; }
    }
}
