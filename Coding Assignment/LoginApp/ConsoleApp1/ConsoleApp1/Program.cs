﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            User user = new User();
            Console.WriteLine("Enter User name:");
            user.Username = Console.ReadLine();
            Console.WriteLine("Enter Password");
            user.Password = Console.ReadLine();

            Login login = new Login();
            login.loginsucessful += OnSucess;
            login.loginfailed += OnFailure;

            var result = login.Authenticate(user);
            login.LogIn(result, user);


        }

        static void OnSucess(LoginEventArgs LoginEvent)              //Handler OnSucees
        {
            Console.WriteLine("Login Successful with ID: {0} and Pass:{1} ..!!!",LoginEvent.Uname,LoginEvent.Pass);
        }

        static void OnFailure(LoginEventArgs LoginEvent)             //Handler OnFailure
        {
            Console.WriteLine("Login Failed with ID: {0} and Pass: {1}..!!!",LoginEvent.Uname,LoginEvent.Pass);
        }
    }
}
