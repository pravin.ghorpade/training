﻿1. Create  a class name  "User" with two properties
	Username (string)
	Password (string)
2.
Create another class name "LogIn" which contains a static method "Authenticate".
which accepts a user object as an argument. The method must check the user details contained in the user object.
The authentication must be succeeded if username and password are flight-flight.
3.
The LogIn method must fire and event named 
"LoginSucessful" if the authentication is suceesfull and "LoginFailed" otherwise.
4.
Create two handlers named "OnSuccess" and "OnFailure" respectively.
Each of these method must accept an instace of a custom class "LoginEventArgs" which contains two properties.
"Uname" and "Pass".
When the event is fired pass an instance of LoginEventArgs class to the event so that the handlers are able to display
Username and Password of the user who attempted to LogIn.
5.
From the "main" method create two instances of user class(One with correct credentials and other with wrong credentials).
Attach the handlers to LoginSucessful and LoginFailed event. 
And call the authenticate method twice by passing both user 
object.

