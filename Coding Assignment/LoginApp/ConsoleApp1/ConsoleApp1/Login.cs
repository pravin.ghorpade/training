﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public delegate void LoginDelegate(LoginEventArgs LoginEvent);
    public class Login
    {
        public event LoginDelegate loginsucessful;
        public event LoginDelegate loginfailed;


        public bool Authenticate(User user)         //Method Authenticate Accepts User Object as a parameter
        {
            if (user.Username == "flights" && user.Password == "flights")
            {
                return true;
            }
            else
                return false;
        }

        public void LogIn(bool result,User u)       //Login Method fired two events login-sucessful and login-failed
        {
            if (result == true)
            {
                LoginEventArgs l = new LoginEventArgs { Uname = u.Username, Pass = u.Password };
                loginsucessful(l);
                
            }
            else
            {
                loginfailed(new LoginEventArgs { Uname = u.Username, Pass = u.Password });
            }
        }
    }
}
