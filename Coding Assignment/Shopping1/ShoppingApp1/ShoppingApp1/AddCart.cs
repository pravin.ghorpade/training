﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp1
{
    public class AddCart
    {
        public void AddToCart()
        {
            var db = new SportItemDataContext();

            ShowProducts sp = new ShowProducts();
            sp.ShowAllProducts();
            Console.WriteLine("\n");

            Console.WriteLine("Enter the ID of the product:");
            int id = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("How many quantity do you want?");
            int quantity = Convert.ToInt32(Console.ReadLine());



            Cart cart = new Cart();
            
            var products5 = from e in db.Products
                            where e.Id == id
                            select e;

            foreach (var item in products5)
            {
                cart.Id = id;
                cart.Name = item.Name;
                cart.Price = item.Price;
                cart.Quantity = quantity;
                cart.Total = item.Price * quantity;
                db.Carts.InsertOnSubmit(cart);

                db.SubmitChanges();
            }



        }
    }
}
