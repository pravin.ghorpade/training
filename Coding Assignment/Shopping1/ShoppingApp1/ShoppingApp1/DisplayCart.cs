﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp1
{
    public class DisplayCart
    {
        public void ShowCart()
        {
            var db = new SportItemDataContext();

            
            var cart = from c in db.Carts

                       select c;
            
            decimal subtotal = 0;
            if (db.Carts.Count()!=0)
            {
                Console.WriteLine("Your Cart");
                Console.WriteLine("---------------------------------------------------------------------------");

                Console.WriteLine(string.Format("{0,-10}\t{1,-10}\t{2,-10}\t{3,-10}\t{4,-10}",
                "Id", "Name", "Price", "Quantity", "Total"));

                Console.WriteLine("---------------------------------------------------------------------------");

                foreach (var item in cart)
                {
                    subtotal += (decimal)item.Total;

                    Console.WriteLine(string.Format("{0,-10}\t{1,-10}\t{2,-10}\t{3,-10}\t{4,-10}",
                            item.Id, item.Name,item.Price, item.Quantity,item.Total));
                }
            }
            else
            {
                Console.WriteLine("Cart is empty..!!!");
            }
            
            Console.WriteLine("Total amount of the Cart is:{0}", +subtotal);
        }
    }
}
