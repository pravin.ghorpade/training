﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp1
{
    public class RemoveFromCart
    {

        public void RemoveCart()
        {
            var db = new SportItemDataContext();
            

            if (db.Carts.Count()!=0)
            {
                DisplayCart dc = new DisplayCart();
                dc.ShowCart();
                Console.WriteLine("\n");
                Console.WriteLine("Enter the ID of the product You want to remove ");
                int id = Convert.ToInt32(Console.ReadLine());

                var result = from item in db.Carts
                             where item.Id == id
                             select item;
                foreach (var item in result)
                {
                    db.Carts.DeleteOnSubmit(item);

                    db.SubmitChanges();
                }

            }
            else
            {
                Console.WriteLine("There is no product in the cart to remove..!!");
            }
        }
    }
}
