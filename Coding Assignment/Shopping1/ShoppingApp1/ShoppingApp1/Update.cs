﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp1
{
    public class Update
    {
        public void UpdateCart()
        {
            var db = new SportItemDataContext();

            DisplayCart dc = new DisplayCart();
            dc.ShowCart();
            Console.WriteLine("\n");

            Console.WriteLine("Enter the ID of the product you want to Update:");
            int id = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter New Quantity.");
            int quantity = Convert.ToInt32(Console.ReadLine());



            Cart cart = new Cart();
            var cart1 = from c in db.Carts
                        where c.Id == id
                        select c;

            var products5 = from e in db.Products
                            where e.Id == id
                            select e;
            foreach (var item1 in cart1)
            {
                if (item1.Id == id)                     //If product is already in the Cart
                {

                    foreach (var item in cart1)
                    {
                        item.Quantity = quantity;
                        item.Total = item.Price * quantity;

                        db.SubmitChanges();
                    }
                }
            }
        }
    }
}
