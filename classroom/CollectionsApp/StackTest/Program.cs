﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace StackTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack stack = new Stack();
            bool aBoolean = true;
            char aChar = '$';
            int aInt = 3556;
            string aString = "Hello";

            //use Push() to add into stack
            stack.Push(aBoolean);
            PrintStack(stack);

            stack.Push(aChar);
            PrintStack(stack);

            stack.Push(aInt);
            PrintStack(stack);

            stack.Push(aString);
            PrintStack(stack);

            Console.WriteLine("First Item in stack is {0} ", stack.Peek());         //stack.peek() is used to display first element of stack.
            PrintStack(stack);

            object o = stack.Pop();                                 //Implicit Unboxing 
            Console.WriteLine(o);

            PrintStack(stack);


            Console.ReadLine();
        }

        private static void PrintStack(Stack stack)
        {
            if (stack.Count == 0)
            {
                Console.WriteLine("Stack is empty..!!!");
            }
            else
            {
                Console.Write("Stack is: ");
                foreach (var item in stack)
                {
                    Console.Write("{0} ", item);
                }
                Console.WriteLine();
            }


        }
    }
}
