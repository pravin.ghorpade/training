﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace ArrayListTest
{
    class Program
    {
        private static readonly string[] colors = { "MAGNETA", "RED", "WHITE", "BLUE", "CYAN" };    //readonly is access modifier
        private static readonly string[] removeColors = { "RED", "WHITE", "BLUE" };


        static void Main(string[] args)
        {
            ArrayList list = new ArrayList(6);              //Capacity is 1
            Console.WriteLine("Capacity: "+list.Capacity);
            //add elements from static array to this list
            foreach (var color in colors)                   //var has become a string because array is of string format
            {
                list.Add(color);
            }

            Console.WriteLine("Capacity: "+list.Capacity);

            ArrayList removeList = new ArrayList(removeColors);
            DisplayInfo(list);

            //Remove colors
            removeColors1(list,removeList);
            Console.WriteLine("\nArrayList after removing colors: ");
            DisplayInfo(list);
            Console.ReadLine();
        }

        private static void removeColors1(ArrayList list, ArrayList removeList)
        {
            for (int count = 0; count < removeList.Count; count++)
            {
                list.Remove(removeList[count]);                              //Boxing
            }
        }

        private static void DisplayInfo(ArrayList list)
        {

            /*arraylist has already IEnumrator so we dont need to get it*/
            //Iterate through list
            foreach (var item in list)
            {
                Console.Write("{0} ",item);
            }

            Console.WriteLine("\nSize: {0}; Capacity: {1}",list.Count,list.Capacity); //ArrayList class has both capacity and count 
            
            int index = list.IndexOf("BLUE");
            if (index != -1)
            {
                Console.WriteLine("The array list contains BLUE at index {0}", index);
            }
            else
            {
                Console.WriteLine("The array list does not contain BLUE anywhere..!!!!!!!!");
            }
        }
    }
}
