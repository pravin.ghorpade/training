﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
namespace CollectionsApp
{
    class Program
    {
        private static int[] intValues = { 1, 2, 3, 4, 5, 6 };
        private static double[] doubleValues = { 8.4, 9.4, 0.2, 7.3, 3.4 };
        private static int[] intValuesCopy;

        static void Main(string[] args)
        {
            //1. Array
            intValuesCopy = new int[intValues.Length];
            Console.WriteLine("Initial Array Value: \n");
            PrintArrays();

            //Sort doubleValues
            Array.Sort(doubleValues);

            //Copy Values from intValues to intValuesCopy array
            Array.Copy(intValues, intValuesCopy, intValues.Length);

            Console.WriteLine("\nArray values after sort and copy: \n ");
            PrintArrays();

            //search 5 in intValues array
            int result = Array.BinarySearch(intValues, 5);
            if (result >= 0)
            {
                Console.WriteLine("5 is found at location {0} in intValues ", result);
            }
            else
            {
                Console.WriteLine("5 not found in intValues.");
            }

            //search 8783 in intValues
            int result1 = Array.BinarySearch(intValues, 8783);
            if (result1 >= 0)
            {
                Console.WriteLine("8783 is found at location {0} in intValues ", result1);
            }
            else
            {
                Console.WriteLine("8783 not found in intValues.");
            }

            Console.ReadLine();
        }

        private static void PrintArrays()
        {
            Console.WriteLine("doubleValues: ");
            IEnumerator enumerator = doubleValues.GetEnumerator();          //String Traverser
            while (enumerator.MoveNext())
            {
                Console.WriteLine(enumerator.Current + " ");

            }
            Console.WriteLine("\nIntValues: ");
            enumerator = intValues.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Console.WriteLine(enumerator.Current + " ");
            }
            Console.WriteLine("\nintValuesCopy: ");
            foreach (var element in intValuesCopy)
            {
                Console.WriteLine(element + " ");

            }

            Console.WriteLine();
        }
    }
}
