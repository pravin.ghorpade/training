﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateApp
{
    delegate void Compute(int a);                                       //delegate declared outside class

    class Program
    {
        static void Main(string[] args)
        {

            Compute c = Square;
            c += Cube;
            c.Invoke(13);
            Console.ReadLine();
        }

        static void Square(int x)                //A static method whose signature should match with delegate
        {
            Console.WriteLine("Sqaure of {0} is {1} ",x,(x * x));
        }

        static void Cube(int y)
        {
            Console.WriteLine("Cube of {0} is {1} ",y,(y*y*y));
        }
            
    }
}
