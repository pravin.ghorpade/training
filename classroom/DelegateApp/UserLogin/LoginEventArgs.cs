﻿namespace UserLogin
{
    public class LoginEventArgs
    {

        public string Uname { get; set; }
        public string Pass { get; set; }
    }
}