﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserLogin
{
 

    class Program
    {
        
        static void Main(string[] args)
        {
            User use = new User();
            Console.WriteLine("Enter user name: ");
            use.Username = Console.ReadLine();
            Console.WriteLine("Enter Password:  ");
            use.Password = Console.ReadLine();

            Login login = new Login();
            login.loginsucess += OnSucess;
            login.loginfail += OnFail;
                   
            var result = login.Authenticate (use);
            login.LoginResult(result, use);
            
        
        }


        static void OnSucess(LoginEventArgs eventArgs)
        {
            Console.WriteLine("Hello {0}, login successful with {1} password.. ", eventArgs.Uname, eventArgs.Pass);
        }

        static void OnFail(LoginEventArgs eventArgs)
        {
            Console.WriteLine("Hello {0}, login failed with {1} password.. ",eventArgs.Uname,eventArgs.Pass);
        }
    }
}
