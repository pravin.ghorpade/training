﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnonymousMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            MyButton button = new MyButton();
            button.Click += delegate ()                     //Instead  of static method anonymous method is used....!!!!
            {
                Console.WriteLine("MyButton clicked..!!");

            };

            button.RaiseEvent();

            Console.ReadLine();
        }
    }


    delegate void ClickHandler();

    class MyButton
    {
        public event ClickHandler Click;

        public void RaiseEvent()
        {
            Click(); 
        }
    }
}
