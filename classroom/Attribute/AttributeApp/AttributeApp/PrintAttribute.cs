﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttributeApp
{
    //This attribute can be applied only to a class.
    //Allow multiple means this attribute can be apllied once on a class.
    //Inherited means a class inherits a class on which this attribute applied,
    //the inherited class will not have this attribute in effect

    [AttributeUsage(AttributeTargets.Class,AllowMultiple =false,Inherited =false)]
    public class PrintAttribute : Attribute
    {
        string destination;
        public PrintAttribute(string dest)
        {
            this.destination = dest;
        }

        public string Destination
        {
            get
            {
                return this.destination;
            }
        }
    }
}
