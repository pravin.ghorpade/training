﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttributeApp
{
    public class Printer
    {
        public static void Print(object obj)
        {
            if (obj is MyDetails)
            {
                MyDetails md = obj as MyDetails;
                Type t = md.GetType();
                foreach (PrintAttribute ps in t.GetCustomAttributes(false))
                {
                    if (ps.Destination=="Printer")
                    {
                        Console.WriteLine("Output Sent to Printer....");
                    }
                    else
                    {
                        Console.WriteLine(md.FirstName+" "+md.LastName);
                    }
                }


            }
        }
    }
}
