﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;                
namespace ConnectedApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (SqlConnection conn=new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=TrainingDB;Integrated Security=True;Pooling=False"))
            {
                conn.Open();                    //to open the connection with database

                //Create required command
                SqlCommand command = new SqlCommand();
                command.CommandText = "select * from Products where Category='Chess'";                 //add your query
                command.CommandType = System.Data.CommandType.Text;
                command.Connection = conn;

                //execute command to get data from database
                SqlDataReader reader = command.ExecuteReader();

                //use data from "reader" object
                while (reader.Read())
                {
                    Console.WriteLine(string.Format("ID: {0}\tName: {1}\tDescription: {2}\tCategory: {3}\tPrice: {4}",reader["Id"],reader["Name"],reader["Description"],reader["Category"],reader["Price"]));
                    Console.WriteLine();
                }


                reader.Close();
                conn.Close();


                Console.ReadLine();
            }
        }
    }
}
