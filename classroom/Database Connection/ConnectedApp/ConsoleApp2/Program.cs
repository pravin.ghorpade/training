﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter following values: ");
            Console.WriteLine("Name: ");
            string name = Console.ReadLine();
            Console.WriteLine("Description:  ");
            string desc = Console.ReadLine();
            Console.WriteLine("Category: ");
            string cat = Console.ReadLine();
            Console.WriteLine("Price: ");
            double price = Convert.ToDouble(Console.ReadLine());

            string insertCommand = string.Format("insert into Products" + "(Name,Description,Category,Price)values('{0}','{1}','{2}','{3}')", name, desc, cat, price);


            using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=TrainingDB;Integrated Security=True;Pooling=False"))
            {
                conn.Open();


                SqlCommand command =new SqlCommand();
                command.CommandText =insertCommand;
                command.CommandType = System.Data.CommandType.Text;
                command.Connection = conn;

                var result = command.ExecuteNonQuery();

                if (result>0)
                {
                    Console.WriteLine("Product inserted Successfully..!!!");
                }
                else
                {
                    Console.WriteLine("Some problem with insertion..");
                }

                conn.Close();



                Console.ReadLine();
            }
            
        }
    }
}
