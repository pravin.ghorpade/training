﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace DisConnectedApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=TrainingDB;Integrated Security=True;Pooling=False"))
            {
                SqlDataAdapter adapter = new SqlDataAdapter("select * from Products", conn);

                DataSet ds = new DataSet();                 //Dataset is a blank object representing Database

                adapter.Fill(ds, "Products");                           //adapter object will open connection,execute command, fetch result and close connection

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Console.WriteLine("Name:{0},\tDescription: {1}\tCategory: {2}\tPrice: {3}",
                        ds.Tables[0].Rows[i]["Name"], ds.Tables[0].Rows[i]["Description"],
                        ds.Tables[0].Rows[i]["Category"], ds.Tables[0].Rows[i]["Price"]);

                }



                //Create a row to add data
                DataRow row = ds.Tables[0].NewRow();

                row["Name"] = "Tennis Ball";
                row["Description"] = "For Child";
                row["Category"] = "Tennis";
                row["Price"] = 34567;

                ds.Tables[0].Rows.Add(row);

                adapter.Update(ds,"Products");               //To udpate changes in database from dataset

                
                //        Console.WriteLine("Coloumns in Products");

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                Console.WriteLine("Name:{0},\tDescription: {1}\tCategory: {2}\tPrice: {3}",
                                    ds.Tables[0].Rows[i]["Name"], ds.Tables[0].Rows[i]["Description"],
                                    ds.Tables[0].Rows[i]["Category"], ds.Tables[0].Rows[i]["Price"]);

                            }

                  /*          foreach (var item in ds.Tables[0].Columns)
                            {
                                Console.WriteLine(item);
                            }

                */
                Console.ReadLine();
            }
        }
    }
}
