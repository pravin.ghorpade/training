﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkApp
{
    class Program
    {
        static void Main(string[] args)
        {
            TrainingDBEntities db = new TrainingDBEntities();

            //Add a product 
            Product prd = new Product { Name = "Tennis Ball", Description = "For Adult", Category = "Tennis", Price = 321 };
            db.Products.Add(prd);
            db.SaveChanges();

            //Update a product
            var prd1 = db.Products.Where(p => p.Id == 11).SingleOrDefault();
            prd1.Price += 100;

           
            //Delete a Product
            var prd = db.Products.Where(p => p.Id == 12).SingleOrDefault();
            db.Products.Remove(prd);
            db.SaveChanges();
           
            
            //Display Tables
            foreach (var item in db.Products.Where(p => p.Price > 30))
            {
                Console.WriteLine("ID: {0}\tName: {1}\tDescription: {2}\tCategory: {3}\tPrice:{4}",
                    item.Id, item.Name, item.Description, item.Category, item.Price);
            }

            Console.ReadLine();
        }
    }
}
