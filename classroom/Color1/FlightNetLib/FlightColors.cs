﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightNetLib
{
   public class FlightColors
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return string.Format("ID: {0} \tName:{1}", ID, Name);
        }
        //public void PrintColor()
        //{
        //    Console.WriteLine("ID: " +ID + "\tName: "+Name);
        //}
    }
}
