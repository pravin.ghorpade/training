﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQTOinMemObjects
{
    public class Customer
    {
        public string CustomerID { get; set; }
        public string City { get; set; }

        public override string ToString()                   //to get simplified output
        {
            return CustomerID + "\t" + City;
        }
    }
}
