﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LINQTOinMemObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            XMLQuery();                 //to call method

            Console.ReadLine();
        }
       /* static void NumQuery()
        {
            var numbers = new int[] { 1, 4, 9, 16, 25, 36 };
            var evenNumbers = from p in numbers
                              where (p % 2) == 0
                              select p;
            Console.WriteLine("Result");
            foreach (var item in evenNumbers)
            {
                Console.WriteLine(item);
            }
        }*/

      /*  static IEnumerable<Customer> CreateCustomer()       //Instead of writing in createCustomer we have retreived from Customers.xml
        {
            return from c in XDocument.Load("Customers.xml").Descendants("Customers").Descendants()
                   select new Customer { City = c.Attribute("City").Value, CustomerID = c.Attribute("CustomerId").Value };
        }*/

 /*       static void ObjectQuery()
        {
            var result = from c in CreateCustomer()
                         where c.City == "Delhi"
                         select c;

            foreach (var item1 in result)
            {
                Console.WriteLine(item1);
            }

        }
*/
        static void XMLQuery()
        {
            var doc = XDocument.Load("Customers.xml");

            var result = from c in doc.Descendants("Customer")
                         where c.Attribute("City").Value == "Delhi"
                         select c;

            /*   foreach (var item in result)
               {
                   Console.WriteLine("{0} \n",item);
               }
               //Console.WriteLine(doc);
            */

            XElement transformDoc = new XElement("Delhite",
                                    from customer in result
                                    select new XElement("Contact",
                                    new XAttribute("ID", customer.Attribute("CustomerID").Value),
                                    new XAttribute("Name",customer.Attribute("ContractName").Value),
                                    new XAttribute("City",customer.Attribute("City").Value)));
            Console.WriteLine("Result:\n{0}",transformDoc);

            transformDoc.Save("Delhite.xml");

        }
       
    }

}

