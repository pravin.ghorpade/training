﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQTOinMemObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            ObjectQuery();                 //to call method

            Console.ReadLine();
        }
        static void NumQuery()
        {
            var numbers = new int[] { 1, 4, 9, 16, 25, 36 };
            var evenNumbers = from p in numbers
                              where (p % 2) == 0
                              select p;
            Console.WriteLine("Result");
            foreach (var item in evenNumbers)
            {
                Console.WriteLine(item);
            }
        }

        static IEnumerable<Customer> CreateCustomer()
        {
            return new List<Customer>
            {
            new Customer { CustomerID=  "Shikhar",City="Delhi" },
            new Customer { CustomerID = "Rohit", City = "Mumbai" },
            new Customer { CustomerID = "Virat", City = "Delhi" },
            new Customer { CustomerID = "Suresh", City = "Gaziabad" },
            new Customer { CustomerID = "MS", City = "Ranchi" },
            new Customer { CustomerID = "Hardik", City = "Gujrat" },
            new Customer { CustomerID = "Bhuvi", City = "Pune" },
            new Customer { CustomerID = "Bumrah", City = "Kolkata" }
            };
        }

        static void ObjectQuery()
        {
            var result = from c in CreateCustomer()
                               where c.City == "Delhi"
                               select c;

        foreach (var item1 in result)
        {
            Console.WriteLine(item1);
        }
                                
        }

    }

}

