﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionApp
{
    class Program
    {
        static void Main(string[] args)
        {
            
            //loading assembly of given path
            Assembly assembly = Assembly.LoadFile(@"C:\training\classroom\Reflection\ConsoleApp1\TripStackLib\bin\Debug\TripStackLib.dll");

            Console.WriteLine("1.Addition");
            Console.WriteLine("2.Subtraction");
            Console.WriteLine("3.Square");
            Console.WriteLine("4.Cube");
            Console.WriteLine("Enter operation number: ");

            int choice = Convert.ToInt32(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    Type t = assembly.GetType("TripStackLib.ComplexMath");
                    //get default constructor
                    ConstructorInfo constructor = t.GetConstructor(Type.EmptyTypes);

                    //Construct an object
                    object instance = constructor.Invoke(null);

                    //get method to be invoked
                    MethodInfo method = t.GetMethod("add");

                    Console.WriteLine("Enter First Number");
                    int x = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter Second Number");
                    int y = Convert.ToInt32(Console.ReadLine());

                    object[] parameters = { x, y };

                    //Invoke method
                    object result = method.Invoke(instance, parameters);

                    //use method's returned data
                    Console.WriteLine("Addition  of {0} and {1} is {2}",x,y,result);
                    break;
                case 2:
                    Type t1 = assembly.GetType("TripStackLib.ComplexMath");
                    //get default constructor
                    ConstructorInfo constructor1 = t1.GetConstructor(Type.EmptyTypes);

                    //Construct an object
                    object instance1 = constructor1.Invoke(null);

                    //get method to be invoked
                    MethodInfo method1 = t1.GetMethod("sub");

                    Console.WriteLine("Enter First Number");
                    int x1 = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter Second Number");
                    int y1 = Convert.ToInt32(Console.ReadLine());

                    object[] parameters1= { x1, y1 };

                    //Invoke method
                    object result1 = method1.Invoke(instance1, parameters1);

                    //use method's returned data
                    Console.WriteLine("Subtraction  of {0} and {1} is {2}", x1, y1, result1);
                    break;
                case 3:
                    Type t2 = assembly.GetType("TripStackLib.SimpleMath");
                    //get default constructor
                    ConstructorInfo constructor2 = t2.GetConstructor(Type.EmptyTypes);

                    //Construct an object
                    object instance2 = constructor2.Invoke(null);

                    //get method to be invoked
                    MethodInfo method2 = t2.GetMethod("Square");

                    Console.WriteLine("Enter Number");
                    int x2 = Convert.ToInt32(Console.ReadLine());
                    
                    object[] parameters2 = { x2 } ;

                    //Invoke method
                    object result2 = method2.Invoke(instance2, parameters2);

                    //use method's returned data
                    Console.WriteLine("Square  of {0} is {1}", x2, result2);
                    break;
                case 4:
                    Type t3 = assembly.GetType("TripStackLib.SimpleMath");
                    //get default constructor
                    ConstructorInfo constructor3 = t3.GetConstructor(Type.EmptyTypes);

                    //Construct an object
                    object instance3 = constructor3.Invoke(null);

                    //get method to be invoked
                    MethodInfo method3 = t3.GetMethod("Cube");

                    Console.WriteLine("Enter Number");
                    int x3 = Convert.ToInt32(Console.ReadLine());

                    object[] parameters3 = { x3 };

                    //Invoke method
                    object result3 = method3.Invoke(instance3, parameters3);

                    //use method's returned data
                    Console.WriteLine("Cube  of {0} is {1}", x3, result3);

                    break;
                default:
                    break;
            }



            Console.ReadLine();
        }
    }
}
