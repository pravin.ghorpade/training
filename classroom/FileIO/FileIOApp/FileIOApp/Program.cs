﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileIOApp
{
    class Program
    {
        static void Main(string[] args)
        {
             //DirectoryInfo di = new DirectoryInfo("C:\\Windows");               //'\'This is escaping escape character i.e '\' 
            DirectoryInfo di = new DirectoryInfo(@"C:\Windows");               // @ sign escapes all the escape character.
            Console.WriteLine("Listing contents of {0} directory: ",di.FullName);

            foreach (DirectoryInfo dir in di.GetDirectories())          //Show folder
            {
               

                Console.WriteLine("Directory Name: " +dir.Name);
                //List all the files in folder
                foreach (var file in dir.GetFiles())
                {
                    Console.WriteLine("File Name:  "+file.Name);
                }
                //Console.WriteLine("File Size in (bytes): " +file.Length);
                Console.WriteLine("Creation Type: "+dir.CreationTime);


                Console.WriteLine();

            }

            Console.ReadLine();
        }


    }
}
