﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StreamReadWriteApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create a file with enable appending
            StreamWriter writer = new StreamWriter(@"C:\training\data.txt",true);   //"true" indicates anable appending

            writer.WriteLine("This is line 1");
            writer.WriteLine("This is line 2");
            writer.WriteLine("This is line 3");
            writer.WriteLine("This is line 4");

            writer.Close();                 //closing the stream

            Console.WriteLine("Press a key to read it back: ");
            Console.ReadLine();

            //Read the content of files in Console
            StreamReader reader = new StreamReader(@"C:\training\data.txt");

            //To Check the end of file
            string line = "";           
             while ((line = reader.ReadLine()) != null)
            {
                Console.WriteLine(line);
            }

            reader.Close();




            Console.ReadLine();
        }
    }
}
