﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryWriterApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //open or create a file
            FileStream fs = File.OpenWrite(@"C:\training\products.txt");      //Openwrite will delete exisiting file 

            BinaryWriter writer = new BinaryWriter(fs);                 //writer is a stream 

            //Prodcut ID
            writer.Write(123);
            
            //Product Name
            writer.Write("product1");

            //Product Price
            writer.Write(50.45);

            //close all the streams before debugging.

            writer.Close();
            fs.Close();

            Console.WriteLine("Product Details are writter to a file.!!!!");

            Console.ReadLine();
        }
    }
}
