﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryReaderApp
{
    class Program
    {
        static void Main(string[] args)
        {

            using (FileStream fs = File.OpenRead(@"C:\training\products.txt"))
            {
                BinaryReader reader = new BinaryReader(fs);

                Console.WriteLine("Product ID:  {0} ", reader.ReadInt32());
                Console.WriteLine("Product Name: {0} ", reader.ReadString());
                Console.WriteLine("Product Price: {0} ", reader.ReadDouble());

                fs.Close();
                reader.Close();
            }

            //When control goes out of using class it will dispose of 'fs' class

            Console.ReadLine();
        }
    }
}
