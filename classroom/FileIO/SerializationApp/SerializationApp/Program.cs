﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace SerializationApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //Creation of "books.txt" file to store all the info of book into 
            FileStream fs = File.OpenWrite(@"C:\training\books.txt");

           //Creation of a formatter for storing books in books.txt
           BinaryFormatter bf = new BinaryFormatter();
           //Create instances of book class
            Book b1 = new Book { BookID = 100, BookName = "Intro to C#", Price = 245 };
            b1.SetPublisher("Packet");

            Book b2 = new Book { BookID = 101, BookName = "Pro C#", Price = 550 };
            b2.SetPublisher("APress");

            List<Book> books = new List<Book> { b1, b2 };
            bf.Serialize(fs, books);

            fs.Close();

            Console.WriteLine("Book stored in books.txt file");


            //Read from file
            fs = File.OpenRead(@"C:\training\books.txt");
            List<Book> b= (List<Book>)bf.Deserialize(fs);                          //Deserialization of file stream object
            fs.Close();

                                                    //Use "List" if more than 1 data is stored of retrive from file
            foreach (var item in b)
            {
                Console.WriteLine(item);
            }

            

          



            Console.ReadLine();

        }
    }
}
