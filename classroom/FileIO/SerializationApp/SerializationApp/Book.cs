﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerializationApp
{
    //Attributes in C#
    [Serializable]                                      //it is to tell CLR that object of this class should be serialized
    public class Book
    {
        public int BookID { get; set; }
        public string BookName { get; set; }
        public int Price { get; set; }

        string publisher;

        public void SetPublisher(string publisher)
        {
            this.publisher = publisher;             //this is used for current object.
        }

        public override string ToString()
        {
            return string.Format("ID: {0}\t Price:{1}\tPublisher: {2}",BookID,BookName,this.publisher);
        }
    }
}
