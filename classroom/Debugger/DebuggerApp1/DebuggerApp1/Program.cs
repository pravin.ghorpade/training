using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebuggerApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Account account = new Account(50.00M);
            Console.WriteLine("Account Balance:{0:C}",account.Balance);
            decimal withdrawAmount;
            Console.WriteLine("Enter withdrawal amount for account: ");
            withdrawAmount = Convert.ToDecimal(Console.ReadLine());

            Console.WriteLine("Withdrawing {0:C} from current balance",withdrawAmount);

            account.Debit(withdrawAmount);

            Console.WriteLine("Account Balance is :{0:C}",account.Balance);
            Console.WriteLine();

            Console.WriteLine("Enter credit amount for account: ");
            decimal creditAmount = Convert.ToDecimal(Console.ReadLine());

            Console.WriteLine("\nAdding {0:C} to account balance",creditAmount);
            account.Balance += creditAmount;
            Console.WriteLine("Account balance is :{0:C}",account.Balance);

            Console.ReadLine();
        }
    }
}
