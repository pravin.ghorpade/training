﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebuggerApp1
{
    class Account
    {
        private decimal balance;
        public decimal Balance
        {
            
            get
            {
                return this.balance;
            }
            set
            {
                if (value>=0)
                {
                    this.balance = value;
                }
            }
        }

        public Account(decimal intBalance)
        {
            Balance = intBalance;
        }

        public void Credit(decimal amount)
        {
            Balance += amount;
        }

        public void Debit(decimal amount)
        {
            if (amount>Balance)
            {
                Console.WriteLine("Debit Amount is exceeded account balance");
            }
            if (amount<=Balance)
            {
                Balance -= amount;
            }
        }
    }
}
