﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Thread1
    {
        public void SaySomething()
        {
            Console.WriteLine("SaySomething() is running on thread number: {0}", Thread.CurrentThread.ManagedThreadId);
        }
    }
}
