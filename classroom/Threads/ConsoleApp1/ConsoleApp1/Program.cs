﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Main() is running on thread number: {0}",Thread.CurrentThread.ManagedThreadId);
            DoSomething();
            Console.WriteLine("Hello");
            Thread1 t = new Thread1();
            t.SaySomething();

            Console.ReadLine();
        }
        static void DoSomething()
        {
            Console.WriteLine("DoSomething() is running on thread number: {0}", Thread.CurrentThread.ManagedThreadId);
        }
    }
}
