﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadApp1
{
    public class TicTock
    {
        public void Tick(bool running)
        {
            lock (this)
            {
                if (!running)
                {
                    //notify any other waiting thread to execute
                    Monitor.Pulse(this);
                    return; 
                }

                Console.WriteLine("Tic");

                //let tock() to run
                Monitor.Pulse(this);

                //wait tock() to complete or notify
                Monitor.Wait(this);
            }

        }

        public void Tock(bool running)
        {
            lock (this)
            {
                if (!running)
                {
                    //notify any other waiting thread to execute
                    Monitor.Pulse(this);
                    return;
                }

                Console.WriteLine("Tock");

                //let tock() to run
                Monitor.Pulse(this);

                //wait tock() to complete or notify
                Monitor.Wait(this); 
            }

        }
    }
}
