﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadApp1
{
    public class MyThread
    {
        public Thread t;
        TicTock tt;

        public MyThread(string name, TicTock ttObj)
        {
            t = new Thread(this.Run);
            this.tt = ttObj;
            t.Name = name;
            t.Start();
        }

        public void Run()
        {
            if (t.Name == "Tick")
            {
                for (int i = 0; i < 5; i++)
                {
                    tt.Tick(true);
                }

            }
            else
            {
                for (int i = 0; i < 5; i++)
                {
                    tt.Tock(true);
                }
                tt.Tock(false);
            }
        }
    }
}
