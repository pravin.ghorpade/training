﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7
{
    public abstract class Shape
    {
        public abstract void Area();
    }
    public class Circle : Shape
    {
        int radius;
        public override void Area()
        {
            Console.WriteLine("Class Circle- Area");
        }

    }
    public class Rectangle : Shape
    {
        int l, b;
        public override void Area()
        {
            Console.WriteLine("Class Rectangle- Area");
        }

    }


}
