﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6b
{
    class President : Employee
    {
        private double km, TA;
        private int Tele;
        public President(string name, double salary, double km1, string designation, double Med, int id, string doj) : base(name, designation, salary, Med, id)
        {
            km = km1;
            TA = (8 * km);
            Tele = 2000;

        }
        public override string ToString()
        {
            return base.ToString() + "Kilometer:" + km + " Tour Allowance:" + TA;
        }


    }
}
