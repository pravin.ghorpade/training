﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6b
{
    class Manager : Employee
    {
        double PetrolA, FoodA, OtherA;
        public Manager(string name, double bsalary, string designation, double Med, int id, string doj) : base(name, designation, bsalary, Med, id)
        {
            PetrolA = (0.08 * bsalary);
            FoodA = (0.13 * bsalary);
            OtherA = (0.03 * bsalary);
        }
        public override string ToString()
        {
            return base.ToString() + "PetrolA:" + PetrolA + "FoodA:" + FoodA + "OtherA:" + OtherA;
        }

    }
}
