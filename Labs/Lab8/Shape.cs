﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    public abstract class Shape
    {
        public abstract void Area();
    }
    public class Circle : Shape, IArea
    {
        int radius;
        public override void Area()
        {
            Console.WriteLine("Class Circle- Area");
        }
        public void Print()
        {
            Console.WriteLine("Interface in Circle");
        }

    }
    public class Rectangle : Shape, IArea
    {
        int l, b;
        public override void Area()
        {
            Console.WriteLine("Class Rectangle- Area");
        }
        public void Print()
        {
            Console.WriteLine("Interface in Rectangle");
        }

    }
    public interface IArea
    {
        void Print();
    }



}
