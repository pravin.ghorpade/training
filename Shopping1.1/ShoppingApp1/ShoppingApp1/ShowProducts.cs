﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp1
{
    public class ShowProducts
    {
         public void ShowAllProducts()
        {
            var db = new SportItemDataContext();
            var result = from a in db.Products
                         select a;

            Console.WriteLine("Product Table");
            Console.WriteLine("---------------------------------------------------------------------------");

            Console.WriteLine(string.Format("{0,-10}\t{1,-10}\t{2,-10}\t{3,-10}\t{4,-10}",
                "Id","Name","Category","Price","Quantity"));

            Console.WriteLine("--------------------------------------------------------------------------");

            foreach (var item in result)
            {

                Console.WriteLine( string.Format("{0,-10}\t{1,-10}\t{2,-10}\t{3,-10}\t{4,-10}", 
                            item.Id, item.Name,item.Category, item.Price, item.Quantity));

            }

        }
    }
}
