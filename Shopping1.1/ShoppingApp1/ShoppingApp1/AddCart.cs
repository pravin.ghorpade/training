﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp1
{
    public class AddCart
    {
        public void AddToCart()
        {
            var db = new SportItemDataContext();

            ShowProducts sp = new ShowProducts();
            sp.ShowAllProducts();
            Console.WriteLine("\n");

            Console.WriteLine("Enter the ID of the product:");
            int id = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Quantity?");
            int quantity = Convert.ToInt32(Console.ReadLine());
            if (quantity > 0)
            {
                Cart cart = new Cart();

                var products = from e in db.Products
                               where e.Id == id
                               select e;

                try
                {
                    foreach (var item in products)
                    {
                        if (item.Quantity > quantity)
                        {
                            cart.Id = id;
                            cart.Name = item.Name;
                            cart.Price = item.Price;
                            cart.Quantity = quantity;
                            cart.Total = item.Price * quantity;
                            db.Carts.InsertOnSubmit(cart);
                            item.Quantity = item.Quantity - quantity;

                            db.SubmitChanges();
                            Console.WriteLine("Product Added Successfully..!!");

                        }
                        else
                        {
                            Console.WriteLine("The required quantity of the product is currently Unavailable.!!");
                        }


                    }
                }
                catch (Exception)
                {

                    Console.WriteLine("The product is already in the Cart.Please Update the quantity.!!");

                }

            }
            else
            {
                Console.WriteLine("Please enter correct quantity.");
            }


        }
    }
}
