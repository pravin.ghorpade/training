﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int flag1 = 0;
            Console.WriteLine("\n*******Console Base Shopping Application*******\n");
            while (flag1 != 1)
            {
                Console.WriteLine("\n");
                Console.WriteLine("------------------------------------------------------------");
                Console.WriteLine("1.Show All Products.");
                Console.WriteLine("2.Show products by Category.");
                Console.WriteLine("3.Add Product to cart");
                Console.WriteLine("4.Update Product Quantity.");
                Console.WriteLine("5.Remove product from Cart");
                Console.WriteLine("6.Show My Cart");
                Console.WriteLine("7.Place Order");
                Console.WriteLine("0 to exit.");
                Console.WriteLine("Enter your choice.");

                int choice = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("");
                switch (choice)
                {
                    case 1:                                             //Display All PRoducts
                        ShowProducts sp = new ShowProducts();
                        sp.ShowAllProducts();
                        break;
                    case 2:                                             //Show Category wise products 
                        Console.WriteLine("Categories:\n1.General Sports\n2.Cricket\n3.Football\n4.Badminton");
                        ProductCategory pc = new ProductCategory();
                        pc.ShowProductByCategory();
                        break;
                    case 3:                                             //Add product to Cart
                        AddCart ac = new AddCart();
                        ac.AddToCart();
                        break;
                    case 4:
                        Update u = new Update();
                        u.UpdateCart();                                 //Update the quantity of product
                        break;
                    case 5:                                             //Remove Product from Cart
                        RemoveFromCart rfc = new RemoveFromCart();
                        rfc.RemoveCart();
                        break;
                    case 6:                                            //Display Cart
                        DisplayCart displaycart = new DisplayCart();
                        displaycart.ShowCart();
                        break;
                    case 7:                                            //Place Order
                        OrderPlaced op = new OrderPlaced();
                        op.PlaceOrder();
                        break;
                    case 0:
                        return;
                    default:
                        break;
                }
            }
            flag1 = 1;

           
        }


        

        

        

        

        
    }
}
