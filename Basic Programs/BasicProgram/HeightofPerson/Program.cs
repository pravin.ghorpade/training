﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeightofPerson
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the height of a person in Cms.");

            try
            {
                float h = Convert.ToDecimal(Console.ReadLine());
                if (h<=0)
                {
                    Console.WriteLine("Please enter positive height");
                }
                if (h <= 125 && h>0)
                {
                    Console.WriteLine("Person is dwarf");
                }
                else if (h > 125 && h < 165)
                {
                    Console.WriteLine("Person has avg.height");
                }
                else if (h > 165)
                {
                    Console.WriteLine("Person is tall");
                }

            }
            catch (Exception)
            {

                Console.WriteLine("Enter correct height");
            }
        }
    }
}
