﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompareDates
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime t1 = new DateTime(2018, 12, 25);
            DateTime t2 = new DateTime(2018, 12, 15, 3, 23, 45);
            int result = DateTime.Compare(t1, t2);
            if (result > 0)
            {
                Console.WriteLine("{1} is earlier than {0}", t1, t2);
            }
            else if (result == 0)
            {
                Console.WriteLine("{0} and {1} are similar", t1, t2);
            }
            else if (result < 0)
            {
                Console.WriteLine("{0} is earlier than  {1}", t1, t2);
            }
        }
    }
}
