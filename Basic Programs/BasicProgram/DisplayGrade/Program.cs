﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DisplayGrade
{
    class Program
    {
        static void Main(string[] args)
        {
            int flag = 0;
            Console.WriteLine("Program to Diplay Grades and Description..");
            while (flag != 1)
            {


                Console.WriteLine("Enter Grades between A to F(0 to exit )");

                try
                {
                    char choice = Convert.ToChar(Console.ReadLine().ToUpper());
                    switch (choice)
                    {
                        case 'A':
                            Console.WriteLine("Excellent");
                            break;
                        case 'B':
                            Console.WriteLine("Very Good");
                            break;
                        case 'C':
                            Console.WriteLine("Good");
                            break;
                        case 'D':
                            Console.WriteLine("Satisfactory");
                            break;
                        case 'E':
                            Console.WriteLine("Unsatisfactory");
                            break;
                        case 'F':
                            Console.WriteLine("Poor");
                            break;
                        case '0':
                            return;
                        default:
                            Console.WriteLine("Invalid Grades");
                            break;
                    }
                }
                catch (Exception)
                {

                    Console.WriteLine("Enter Valid Grade.");
                }

            }
            flag = 1;

        }
    }
}
