﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreatestNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter two numbers: ");
            try
            {
                int a = Convert.ToInt32(Console.ReadLine());
                int b = Convert.ToInt32(Console.ReadLine());
                if (a > b)
                {
                    Console.WriteLine("{0} is greater than {1}", a, b);
                }
                else if (a == b)
                {
                    Console.WriteLine("Numbers are equal");
                }
                else if (a < b)
                {
                    Console.WriteLine("{0} is greater than {1}", b, a);
                }

            }
            catch (Exception)
            {

                Console.WriteLine("Invalid Input...!!");
            }
        }
    }
}
