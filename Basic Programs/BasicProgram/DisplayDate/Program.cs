﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DisplayDate
{
    class Program
    {
        static int Main(string[] args)
        {
            //sDateTime dateTime = DateTime.Now;

            Console.WriteLine("Date and Time:  {0}", DateTime.Now);
            Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd"));
            Console.WriteLine(DateTime.Now.ToString("ddd-dd-MMM-yy"));
            Console.WriteLine(DateTime.Now.ToString("M/d/yyyy"));
            Console.WriteLine(DateTime.Now.ToString("M/d/yy"));
            Console.WriteLine(DateTime.Now.ToString("MM:dd:yyyy"));
            Console.WriteLine(DateTime.Now.ToString("yyyy/MM/dd"));

            Console.Read();
            return 0;
        }
    }
}
