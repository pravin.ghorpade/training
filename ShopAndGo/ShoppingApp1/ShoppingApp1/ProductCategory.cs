﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp1
{
    public class ProductCategory
    {
        public void ShowProductByCategory()
        {

            int choice1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("\n");
            var db = new SportItemDataContext();

            switch (choice1)
            {
                case 1:                                 //Category Sports

                    var products1 = from a in db.Products
                                    where a.Category == "Sport"
                                    select a;
                    Console.WriteLine("-----------------------------------------------------------------");
                    Console.WriteLine(string.Format("{0,-10}\t{1,-10}\t{2,-10}\t{3,-10}\t",
                        "Id", "Name", "Price", "Quantity"));
                    Console.WriteLine("-----------------------------------------------------------------");
                    foreach (var item in products1)
                    {
                        Console.WriteLine(
                            string.Format("{0,-10}\t{1,-10}\t{2,-10}\t\t{3,-10}", item.Id, item.Name, item.Price, item.Quantity));
                    }

                    break;
                case 2:                             //Category Cricket
                    var products2 = from b in db.Products
                                    where b.Category == "Cricket"
                                    select b;
                    Console.WriteLine("-----------------------------------------------------------------");
                    Console.WriteLine(string.Format("{0,-10}\t{1,-10}\t{2,-10}\t{3,-10}\t",
                        "Id", "Name","Price", "Quantity"));
                    Console.WriteLine("-----------------------------------------------------------------");

                    foreach (var item in products2)
                    {
                        Console.WriteLine(
                            string.Format("{0,-10}\t{1,-10}\t{2,-10}\t\t{3,-10}", item.Id, item.Name, item.Price, item.Quantity));
                    }
                    break;

                case 3:                             //Category Football
                    var products3 = from c in db.Products
                                    where c.Category == "Football"
                                    select c;
                    Console.WriteLine("-----------------------------------------------------------------");
                    Console.WriteLine(string.Format("{0,-10}\t{1,-10}\t{2,-10}\t{3,-10}\t",
                        "Id", "Name", "Price", "Quantity"));
                    Console.WriteLine("-----------------------------------------------------------------");
                    foreach (var item in products3)
                    {
                        Console.WriteLine(
                            string.Format("{0,-10}\t{1,-10}\t{2,-10}\t\t{3,-10}", item.Id, item.Name, item.Price, item.Quantity));
                    }
                    break;

                case 4:                         //Category Badminton

                    var products4 = from d in db.Products
                                    where d.Category == "Badminton"
                                    select d;
                    Console.WriteLine("-----------------------------------------------------------------");
                    Console.WriteLine(string.Format("{0,-10}\t{1,-10}\t{2,-10}\t{3,-10}\t",
                        "Id", "Name", "Price", "Quantity"));
                    Console.WriteLine("-----------------------------------------------------------------");
                    foreach (var item in products4)
                    {
                        Console.WriteLine(
                            string.Format("{0,-10}\t{1,-10}\t{2,-10}\t\t{3,-10}", item.Id, item.Name, item.Price, item.Quantity));
                    }
                    break;

                default:
                    break;
            }
        }
    }
}
