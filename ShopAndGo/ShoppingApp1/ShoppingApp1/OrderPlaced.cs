﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp1
{
    public class OrderPlaced
    {
        public void PlaceOrder()
        {
            var db = new SportItemDataContext();
            var result = from item in db.Carts
                         select item;

            if (db.Carts.Count() != 0)
            {
                foreach (var item in result)
                {
                    db.Carts.DeleteAllOnSubmit(result);
                    db.SubmitChanges();

                }
                Console.WriteLine("Your Order is Placed..!!!\nThank you for ordering..!!");

            }
            else
            {
                Console.WriteLine("Nothing in the Cart..!!\nOrder Can't be placed..");
            }
        }

    }
}
