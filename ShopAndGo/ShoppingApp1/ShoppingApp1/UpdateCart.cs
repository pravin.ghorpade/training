﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp1
{
    public class UpdateCart
    {

        public void UpdateQuantity(int id)
        {
            Console.WriteLine("Product you enterd is already in the cart..!!\nEnter new quantity..");
            int quantity = Convert.ToInt32(Console.ReadLine());
            var db = new SportItemDataContext();
            var cart = from c in db.Carts
                    where c.Id == id
                    select c;
            foreach (var item in cart)
            {
                item.Quantity = quantity;
                item.Total = item.Price * quantity;

                db.SubmitChanges();
            }
        }
        

    }
}
