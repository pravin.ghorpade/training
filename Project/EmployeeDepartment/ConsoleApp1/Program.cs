﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace ConsoleApp1
{
    class Program
    {
        static List<Department> departments;
        static List<Employee> employees;

        //static int deptID = 0;
        static int empID = 0;

        static void Main(string[] args)
        {


            departments = new List<Department>();
            employees = new List<Employee>();
            int mainflag = 0;
            while (mainflag != 1)
            {


                Console.WriteLine("\n What do you want to do ?");
                Console.WriteLine("1. Create Department");
                Console.WriteLine("2.Create Employee");
                Console.WriteLine("3.Show All Department Details");
                Console.WriteLine("4.Show All Employee Details");
                Console.WriteLine("5.Show Particular Department Details");
                Console.WriteLine("6.Show Particular Employee Details");
                Console.WriteLine("7.Update Employee");
                Console.WriteLine("8.Delete Department");
                Console.WriteLine("9.Delete Employee");
                Console.WriteLine("0. Exit");
                int choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        Department d = new Department();
                        Console.WriteLine("Enter Department ID");
                        d.deptID = Convert.ToInt32(Console.ReadLine());
                        int flag = 0;
                        foreach (var item in departments)
                        {
                            // Console.WriteLine(item);
                            //Console.WriteLine(d.deptID);
                            if (item.deptID == d.deptID)
                            {
                                Console.WriteLine("Department is present");
                                flag = 1;
                            }

                        }
                        if (flag == 0)
                        {
                            Console.WriteLine("Enter Department Name");
                            d.deptName = Console.ReadLine();
                            Console.WriteLine("Enter Location");
                            d.Location = Console.ReadLine();
                            departments.Add(d);
                        }
                        break;
                    case 2:
                        Employee e = new Employee();
                        empID = empID + 1;
                        e.empID = empID;
                        Console.WriteLine("Enter your Name");
                        e.empName = Console.ReadLine();
                        Console.WriteLine("Enter the option of Designation as below");
                        Console.WriteLine("1. Employee");
                        Console.WriteLine("2. Manager");
                        Console.WriteLine("3. President");
                        int ch = Convert.ToInt32(Console.ReadLine());
                        if (ch == 1)
                        {
                            e.Designation = "Employee";
                        }
                        if (ch == 2)
                        {
                            e.Designation = "Manager";
                        }
                        if (ch == 3)
                        {
                            e.Designation = "President";
                        }
                        Console.WriteLine("Enter your Basic Salary");
                        e.BasicSalary = Convert.ToInt32(Console.ReadLine());
                        e.hra = (8 * e.BasicSalary) / 100;
                        e.pf = (12 * e.BasicSalary) / 100;
                        e.gs = e.BasicSalary + e.hra;
                        e.ns = e.gs - (e.hra + e.pf);
                        Console.WriteLine("Enter the DepartmentID");
                        e.depID = Convert.ToInt32(Console.ReadLine());
                        employees.Add(e);
                    break;
                    case 3:
                        if (departments.Count == 0)
                        {
                            Console.WriteLine("Dont have any department");
                        }
                        else
                        {
                            foreach (var item in departments)
                            {
                                Console.WriteLine(item);
                            }

                        }
                        break;
                    case 4:
                        if (employees.Count == 0)
                        {
                            Console.WriteLine("Dont have any employee information");
                        }
                        else
                        {
                            foreach (var item in employees)
                            {
                                Console.WriteLine(item);
                            }

                        }

                    break;
                    case 5:
                        Console.WriteLine("Enter ID of the Department  to be searched");
                        int did = Convert.ToInt32(Console.ReadLine());
                        int ind = -1;
                        int flg = 0;
                        foreach (var item in departments)
                        {
                            if (departments.Count == 0)
                            {
                                Console.WriteLine("Department with this ID doesn't exist.!!!");
                            }

                            ind += 1;
                            if (item.deptID == did)
                            {
                                Console.WriteLine(departments[ind]);
                                flg = 1;
                                break;
                            }
                        }
                        if (flg == 0)
                        {
                            Console.WriteLine("Given Department ID not present in the list!!!!");
                        }
                        break;
                    case 6:
                        Console.WriteLine("Enter the employee name to be searched");
                        string opt = Console.ReadLine();
                        ind = -1;
                        if (employees.Count == 0)
                        {
                            Console.WriteLine("Employee doesnt contain any information");
                        }

                        foreach (var item in employees)
                        {
                            ind += 1;
                            if (item.empName == opt)
                            {
                                Console.WriteLine(employees[ind]);
                            }
                        }
                     break;

                    case 7:
                        Console.WriteLine("Enter Employee Name to be updated");
                        string opt1 = Console.ReadLine();
                        ind = -1;
                        foreach (var item in employees)
                        {
                            ind += 1;
                            if (item.empName == opt1)
                            {
                                int flag1 = 0;
                                while (flag1 != 1)
                                {
                                    Console.WriteLine("Enter what you need to update");
                                    Console.WriteLine("1. Employee Name");
                                    Console.WriteLine("2. Designation");
                                    Console.WriteLine("3. Basic Salary");
                                    Console.WriteLine("4. Department ID");
                                    Console.WriteLine("0. If dont want to update anything");
                                    int ch4 = Convert.ToInt32(Console.ReadLine());
                                    if (ch4 == 1)
                                    {
                                        Console.WriteLine("Enter the name you want to change");
                                        string val = Console.ReadLine();
                                        employees[ind].empName = val;
                                    }
                                    if (ch4 == 2)
                                    {
                                        Console.WriteLine("Enter the option of Designation as below");
                                        Console.WriteLine("1. Employee");
                                        Console.WriteLine("2. Manager");
                                        Console.WriteLine("3. President");
                                        int cho = Convert.ToInt32(Console.ReadLine());
                                        if (cho == 1)
                                        {
                                            employees[ind].Designation = "Employee";
                                        }
                                        if (cho == 2)
                                        {
                                            employees[ind].Designation = "Manager";
                                        }
                                        if (cho == 3)
                                        {
                                            employees[ind].Designation = "President";
                                        }
                                    }
                                    if (ch4 == 3)
                                    {
                                        Console.WriteLine("Enter your change in salary");
                                        int newsal = Convert.ToInt32(Console.ReadLine());
                                        double hra2 = (8 * newsal) / 100;
                                        double pf2 = (12 * newsal) / 100;
                                        double gs2 = newsal + hra2;
                                        double ns2 = gs2 - (hra2 + pf2);
                                        employees[ind].BasicSalary = newsal;
                                        employees[ind].hra = hra2;
                                        employees[ind].pf = pf2;
                                        employees[ind].gs = gs2;
                                        employees[ind].ns = ns2;

                                    }
                                    if (ch4 == 4)
                                    {
                                        Console.WriteLine("Enter the changed Department ID");
                                        int depid = Convert.ToInt32(Console.ReadLine());
                                        employees[ind].depID = depid;
                                    }
                                    if (ch4 == 0)
                                    {
                                        flag1 = 1;

                                    }
                                }
                            }
                        }
                    break;
                    case 8:
                        Console.WriteLine("Enter the department ID you want to delete");
                        int o = Convert.ToInt32(Console.ReadLine());
                        int flag2 = 0;
                        foreach (var item in employees)
                        {
                            if (item.depID == o)
                            {
                                Console.WriteLine("Sorry cannot delete the information");
                                flag = 1;
                            }
                        }
                        if (flag2 == 0)
                        {
                            int newflag = 0;
                            int ind1 = -1;
                            foreach (var item in departments)
                            {

                                ind1 += 1;
                                if (item.deptID == o)
                                {
                                    departments.RemoveAt(ind1);
                                    newflag = 1;
                                }
                            }
                            if (newflag == 0)
                            {
                                Console.WriteLine("Department ID not present");
                            }
                        }
                    break;
                    case 9:
                        Console.WriteLine("Enter the employee name you want to delete");
                        string name = Console.ReadLine();
                        int flag3 = 0;
                        int ind2 = -1;
                        foreach (var item in employees)
                        {
                            ind2 += 1;
                            if (item.empName == name)
                            {
                                employees.RemoveAt(ind2);
                                flag3 = 1;
                            }

                        }
                        if (flag3 == 0)
                        {
                            Console.WriteLine("Sorry Name not found");
                        }
                    break;
                    default:
                        break;
                }

            }
            mainflag = 1;
        }
    }
}
