﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp1
{
    //Class to handle all the operation related to Cart 
    public class ShoppingCart
    {
        //Method to add products in the cart
        public void AddToCart()
        {
            var db = new SportItemDataContext();

            ShoppingProduct sp = new ShoppingProduct();
            sp.ShowAllProducts();                           //show all products to customer for selecting
            Console.WriteLine("\n");

            Console.WriteLine("Enter the ID of the product:");
            int id = Convert.ToInt32(Console.ReadLine());

            if (id > 0 && id < 14)
            {
                Console.WriteLine("Quantity?");
                int quantity = Convert.ToInt32(Console.ReadLine());
                if (quantity > 0)
                {
                    Cart cart = new Cart();

                    var products = from e in db.Products
                                   where e.Id == id
                                   select e;

                    try
                    {
                        foreach (var item in products)
                        {
                            if (item.Quantity > quantity)
                            {
                                cart.Id = id;
                                cart.Name = item.Name;
                                cart.Price = item.Price;
                                cart.Quantity = quantity;
                                cart.Total = item.Price * quantity;
                                db.Carts.InsertOnSubmit(cart);
                                item.Quantity = item.Quantity - quantity;

                                db.SubmitChanges();
                                Console.WriteLine("Product Added Successfully..!!");

                            }
                            else
                            {
                                Console.WriteLine("The required quantity of the product is currently Unavailable.!!");
                            }
                        }
                    }
                    catch (Exception)
                    {

                        Console.WriteLine("The product is already in the Cart.Please Update the quantity.!!");
                    }
                }
                else
                {
                    Console.WriteLine("Please enter correct quantity.");
                }

            }
            else
            {
                Console.WriteLine("Please enter correct ID");
            }

        }

        //Method to Show details of the Cart
        public void ShowCart()
        {
            var db = new SportItemDataContext();


            var cart = from c in db.Carts               //select all the items of the cart
                       select c;

            decimal subtotal = 0;
            if (db.Carts.Count() != 0)
            {
                Console.WriteLine("Your Cart");
                Console.WriteLine("---------------------------------------------------------------------------");

                Console.WriteLine(string.Format("{0,-10}\t{1,-10}\t{2,-10}\t{3,-10}\t{4,-10}",
                "Id", "Name", "Price", "Quantity", "Total"));

                Console.WriteLine("---------------------------------------------------------------------------");

                foreach (var item in cart)
                {
                    subtotal += (decimal)item.Total;                    //add price of all the items in the cart

                    Console.WriteLine(string.Format("{0,-10}\t{1,-10}\t{2,-10}\t{3,-10}\t{4,-10}",
                            item.Id, item.Name, item.Price, item.Quantity, item.Total));
                }
            }
            else
            {
                Console.WriteLine("Cart is empty..!!!");
            }

            Console.WriteLine("Total amount of the Cart is:{0}", +subtotal);
        }

        //Method to place order
        public void PlaceOrder()
        {
            var db = new SportItemDataContext();
            var result = from item in db.Carts
                         select item;

            if (db.Carts.Count() != 0)      //if there is at least one item in the cart
            {
                foreach (var item in result)
                {
                    db.Carts.DeleteAllOnSubmit(result);
                    db.SubmitChanges();

                }
                Console.WriteLine("Your Order is Placed..!!!\nThank you for ordering..!!");

            }
            else
            {
                Console.WriteLine("Nothing in the Cart..!!\nOrder Can't be placed..");
            }
        }

        //Method to Remove Item from the Cart
        public void RemoveCart()
        {
            var db = new SportItemDataContext();


            if (db.Carts.Count() != 0)          //execute when the cart is not empty
            {

                ShowCart();
                Console.WriteLine("\n");
                Console.WriteLine("Enter the ID of the product You want to remove ");
                int id = Convert.ToInt32(Console.ReadLine());
                var result = from item in db.Carts
                             where item.Id == id
                             select item;
                var result1 = from p in db.Products
                              select p;
                foreach (var item in result)            //remove product from Cart table
                {
                    db.Carts.DeleteOnSubmit(item);

                    db.SubmitChanges();

                    foreach (var item1 in result1)              //Update the quantity of the product in the products table
                    {
                        item1.Quantity = item1.Quantity + item.Quantity;
                        db.SubmitChanges();
                    }
                }
                Console.WriteLine("Product is Removed Successfully.");
            }

            else
            {
                Console.WriteLine("There is no product in the cart to remove..!!");
            }
        }

        //Method to Update Cart
        public void UpdateCart()
        {
            var db = new SportItemDataContext();


            ShowCart();
            Console.WriteLine("\n");

            if (db.Carts.Count() == 0)                  //If product with given id is not in the cart
            {
                Console.WriteLine("Please add the product.");
            }
            else
            {
                Console.WriteLine("Enter the ID of the product you want to Update:");
                int id = Convert.ToInt32(Console.ReadLine());
                var cart1 = from c in db.Carts
                            where c.Id == id
                            select c;
                Console.WriteLine("Enter New Quantity.");
                int quantity = Convert.ToInt32(Console.ReadLine());

                foreach (var item1 in cart1)
                {
                    if (item1.Id == id)                     //If product is already in the Cart
                    {

                        foreach (var item in cart1)
                        {
                            item.Quantity = quantity;
                            item.Total = item.Price * quantity;

                            db.SubmitChanges();
                        }
                    }
                }

                Console.WriteLine("Quantity is modified...!!");
            }

        }

    }
}
