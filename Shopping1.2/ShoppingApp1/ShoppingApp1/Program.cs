﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp1
{
    class Program
    {


        static void Main(string[] args)
        {
            var db = new SportItemDataContext();
            ShoppingCart sc = new ShoppingCart();
            ShoppingProduct sp = new ShoppingProduct();
            int flag1 = 0;
            Console.WriteLine("\n*******Console Base Shopping Application*******\n");
            while (flag1 != 1)
            {
                if (db.Carts.Count()!=0)
                {
                    Console.WriteLine("\n");
                    Console.WriteLine("------------------------------------------------------------");
                    Console.WriteLine("1.Show All Products.");
                    Console.WriteLine("2.Show products by Category.");
                    Console.WriteLine("3.Add Product to cart");
                    Console.WriteLine("4.Update Product Quantity.");
                    Console.WriteLine("5.Remove product from Cart");
                    Console.WriteLine("6.Show My Cart");
                    Console.WriteLine("7.Place Order");
                    Console.WriteLine("0 to exit.");
                    Console.WriteLine("Enter your choice.");
                }
                else
                {
                    Console.WriteLine("\n");
                    Console.WriteLine("------------------------------------------------------------");
                    Console.WriteLine("1.Show All Products.");
                    Console.WriteLine("2.Show products by Category.");
                    Console.WriteLine("3.Add Product to cart");
                    Console.WriteLine("0 to exit.");
                    Console.WriteLine("Enter your choice.");
                }
                try
                {
                    int choice = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("");
                    switch (choice)
                    {
                        case 1:                                             //Display All PRoducts
                            Console.Clear();
                            sp.ShowAllProducts();
                            break;
                        case 2:                                             //Show Category wise products 
                            Console.Clear();
                            Console.WriteLine("Categories:\n1.General Sports\n2.Cricket\n3.Football\n4.Badminton");
                            sp.ShowProductByCategory();
                            break;
                        case 3:                                             //Add product to Cart
                            Console.Clear();
                            sc.AddToCart();
                            break;
                        case 4:
                            Console.Clear();
                            sc.UpdateCart();                                 //Update the quantity of product
                            break;
                        case 5:                                             //Remove Product from Cart
                            Console.Clear();
                            sc.RemoveCart();
                            break;
                        case 6:                                            //Display Cart
                            Console.Clear();
                            sc.ShowCart();
                            break;
                        case 7:                                            //Place Order
                            Console.Clear();
                            sc.PlaceOrder();
                            break;
                        case 0:
                            return;
                        default:
                            Console.WriteLine("Please enter a correct choice");
                            break;
                    }

                }
                catch (Exception)
                {

                    Console.WriteLine("Invalid Input..");
                }
            }
            flag1 = 1;

        }
    }
}
