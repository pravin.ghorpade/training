﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp1
{
    //Class to handle all the operation related to Products
    public class ShoppingProduct
    {

        //Method is display all the products. 
        public void ShowAllProducts()
        {
            var db = new SportItemDataContext();
            var result = from a in db.Products
                         select a;

            Console.WriteLine("Product Table");
            Console.WriteLine("---------------------------------------------------------------------------");

            Console.WriteLine(string.Format("{0,-10}\t{1,-10}\t{2,-10}\t{3,-10}\t{4,-10}",
                "Id", "Name", "Category", "Price", "Quantity"));

            Console.WriteLine("--------------------------------------------------------------------------");

            foreach (var item in result)
            {

                Console.WriteLine(string.Format("{0,-10}\t{1,-10}\t{2,-10}\t{3,-10}\t{4,-10}",
                            item.Id, item.Name, item.Category, item.Price, item.Quantity));

            }

        }
        
        //method to show products by category
        public void ShowProductByCategory()
        {

            int choice1 = Convert.ToInt32(Console.ReadLine());          //select category
            Console.WriteLine("\n");
            var db = new SportItemDataContext();

            switch (choice1)
            {
                case 1:                                 // For Category Sports

                    var products1 = from a in db.Products
                                    where a.Category == "Sport"
                                    select a;
                    Console.WriteLine("-----------------------------------------------------------------");
                    Console.WriteLine(string.Format("{0,-10}\t{1,-10}\t{2,-10}\t{3,-10}\t",
                        "Id", "Name", "Price", "Quantity"));
                    Console.WriteLine("-----------------------------------------------------------------");
                    foreach (var item in products1)
                    {
                        Console.WriteLine(
                            string.Format("{0,-10}\t{1,-10}\t{2,-10}\t\t{3,-10}", item.Id, item.Name, item.Price, item.Quantity));
                    }

                    break;
                case 2:                             // For Category Cricket
                    var products2 = from b in db.Products
                                    where b.Category == "Cricket"
                                    select b;
                    Console.WriteLine("-----------------------------------------------------------------");
                    Console.WriteLine(string.Format("{0,-10}\t{1,-10}\t{2,-10}\t{3,-10}\t",
                        "Id", "Name", "Price", "Quantity"));
                    Console.WriteLine("-----------------------------------------------------------------");

                    foreach (var item in products2)
                    {
                        Console.WriteLine(
                            string.Format("{0,-10}\t{1,-10}\t{2,-10}\t\t{3,-10}", item.Id, item.Name, item.Price, item.Quantity));
                    }
                    break;

                case 3:             //For Category Football
                    var products3 = from c in db.Products
                                    where c.Category == "Football"
                                    select c;
                    Console.WriteLine("-----------------------------------------------------------------");
                    Console.WriteLine(string.Format("{0,-10}\t{1,-10}\t{2,-10}\t{3,-10}\t",
                        "Id", "Name", "Price", "Quantity"));
                    Console.WriteLine("-----------------------------------------------------------------");
                    foreach (var item in products3)
                    {
                        Console.WriteLine(
                            string.Format("{0,-10}\t{1,-10}\t{2,-10}\t\t{3,-10}", item.Id, item.Name, item.Price, item.Quantity));
                    }
                    break;

                case 4:
                    //For Category Badminton
                    var products4 = from d in db.Products
                                    where d.Category == "Badminton"
                                    select d;
                    Console.WriteLine("-----------------------------------------------------------------");
                    Console.WriteLine(string.Format("{0,-10}\t{1,-10}\t{2,-10}\t{3,-10}\t",
                        "Id", "Name", "Price", "Quantity"));
                    Console.WriteLine("-----------------------------------------------------------------");
                    foreach (var item in products4)
                    {
                        Console.WriteLine(
                            string.Format("{0,-10}\t{1,-10}\t{2,-10}\t\t{3,-10}", item.Id, item.Name, item.Price, item.Quantity));
                    }
                    break;

                default:
                    Console.WriteLine("Please enter correct choice");
                    break;
            }
        }

    }
}
